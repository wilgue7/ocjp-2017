/**
 * Created by Wilmer on 05/06/2017.
 */
public class Variables {
    private int precio; // Las instrucciones y declaraciones finalizan con ;
    private int importe_acumulado;
    private String profesor;
    private String aula;
    private int capacidad;
    private boolean funciona;
    private boolean esVisible;
    private float diametro;
    private float peso;
    private short edad;
    private long masa;
    private char letra1;

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getImporte_acumulado() {
        return importe_acumulado;
    }

    public void setImporte_acumulado(int importe_acumulado) {
        this.importe_acumulado = importe_acumulado;
    }

    public String getProfesor() {
        return profesor;
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public boolean isFunciona() {
        return funciona;
    }

    public void setFunciona(boolean funciona) {
        this.funciona = funciona;
    }

    public boolean isEsVisible() {
        return esVisible;
    }

    public void setEsVisible(boolean esVisible) {
        this.esVisible = esVisible;
    }

    public float getDiametro() {
        return diametro;
    }

    public void setDiametro(float diametro) {
        this.diametro = diametro;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    public long getMasa() {
        return masa;
    }

    public void setMasa(long masa) {
        this.masa = masa;
    }

    public char getLetra1() {
        return letra1;
    }

    public void setLetra1(char letra1) {
        this.letra1 = letra1;
    }

    public void inicializar(){
        precio = 42; // Entero tipo int. Un número sin punto decimal se interpreta normalmente como int.
        importe_acumulado = 210; // Entero tipo int
        profesor = "Wilmer Guevara"; // Tipo String
        aula = "A-44"; // Tipo String
        capacidad = 1500; // Entero tipo int
        funciona = true; // Tipo boolean
        esVisible = false; // Tipo boolean
        diametro = 34.25f; // Tipo float. Una f o F final indica que es float.
        peso=87; // Tipo double. Un número con punto decimal se interpreta normalmente como double.
        edad = 19; // Entero tipo short
        masa = 178823411L; // Entero tipo long. Una l o L final indica que es long.
        letra1 = 'h'; // Tipo char (carácter). Se escribe entre comillas simples.
    }

}
