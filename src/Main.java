public class Main {

    public static void main(String[] args) {
        Variables v= new Variables();
        /**
         * operadores
         */
        v.inicializar();
        /**
         * igual
         */
        if (v.getEdad()==18){
            System.out.println("usted tiene 18 años: "+v.getEdad());
        }
        /**
         * menor
         */
        if (v.getEdad()<18){
            System.out.println("usted es menor de edad: "+v.getEdad());
        }
        /**
         * dieferente
         */
        if (v.getEdad()>18){
            System.out.println("usted es mayor de edad: "+v.getEdad());
        }
        /**
         * diferente con operacion multiplicar
         */
        if (v.getImporte_acumulado()*3 != 30) {
            System.out.println("El calculo es diferente de 30");
        }
        /**
         * AND
         */
        if(v.getCapacidad()<30 && v.getPrecio()>50){
            System.out.println("ustede puede matricularce en este curso");
        }
        /**
         * or
         */
        if(v.getProfesor()=="Wilmer Guevara" || v.getAula()=="A-43"){
            System.out.println("su profesor es:"+v.getProfesor());
            System.out.println("O su aula es "+v.getAula());
        }
        /**
         *suma
         */
        System.out.println("suma:"+(v.getPrecio()+50));
        /**
         * resta
         */
        System.out.println("resta:"+(v.getPrecio()-v.getImporte_acumulado()));
        /**
         * Division
         */
        System.out.println("Division:"+(v.getDiametro()/3));
        /**
         * casting
         */
        Persona p=(Persona)v;
        /**
         * conversion por asignacion
         */

        double f= v.getCapacidad()+3.14;
        System.out.println("mi variable f: "+f);
    }
}
